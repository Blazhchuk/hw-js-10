"use strict"

/*

1.  Є такі способи створення та додавання нових DOM-елементів:
    document.createElement() - Створює новий елемент з вказаним тегом.
    document.createTextNode() - Створює новий текстовий вузол, який можна додати до елмента.
    Node.appendChild() - додає вузол в якості останнього дочірнього вузла до вказаного вузла.
    Node.insertBefore() - додає вузол до вказаного вузла перед вказаним дочірнім вузлом.
    Element.innerHTML - дозволяє отримувати або встановлювати HTML-контент елемента.
    Element.insertAdjacentHTML() - вставляє рядок HTML в вказане місце щодо елемента.

2.  Спочатку потрібно знайти елемент за допомогою querySelector/getElementBy...
    let n = document.querySelector('.navigator');
    Потім прописуємо команду remove();
    n.remove();

3.  Існують такі методи вставки DOM-елементів перед або після іншого DOM-елемента:
    Node.insertBefore(newNode, referenceNode) - Цей метод вставляє newNode перед referenceNode в списку дочірніх вузлів батьківського вузла. Якщо referenceNode є null, newNode додається в кінець списку.
    ParentNode.append(...nodesOrDOMStrings): Цей метод вставляє набір Node об’єктів або DOMString об’єктів в кінець ParentNode. DOMString об’єкти вставляються як Text вузли.
    ParentNode.prepend(...nodesOrDOMStrings): Цей метод вставляє набір Node об’єктів або DOMString об’єктів на початок ParentNode. DOMString об’єкти вставляються як Text вузли.
    ChildNode.after(...nodesOrDOMStrings) та ChildNode.before(...nodesOrDOMStrings): Ці методи вставляють набір Node або DOMString об’єктів відповідно перед або після ChildNode в списку дочірніх вузлів батьківського вузла.

*/


// ex 1

let link = document.createElement('a');
let footer = document.querySelector('footer');
let paragraph = footer.querySelector('p');

link.textContent = 'Learn More';
link.href = '#';
paragraph.after(link)


// ex 2

let select = document.createElement('select');
let main = document.querySelector('main');
let features = document.querySelector('.features');

select.id = 'rating';
features.before(select);

for (let i = 4; i >= 1; i--) {
    let option = document.createElement('option');
    option.value = i;
    option.text = i > 1 ? `${i} Stars` : `${i} Star`;
    select.appendChild(option);
}
